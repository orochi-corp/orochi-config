set -Ux LC_ALL en_AU.UTF-8
set -Ux LC_CTYPE en_AU.UTF-8

set __fish_git_prompt_color $fish_color_command
set __fish_git_prompt_color_dirtystate yellow
set __fish_git_prompt_color_stagedstate green
set __fish_git_prompt_color_merging red
set __fish_git_prompt_showdirtystate true

function fish_prompt --description 'Write out the prompt'
  set -l last_pipestatus $pipestatus
  set -l normal (set_color normal)

  # Color the prompt differently when we're root
  set -l color_cwd $fish_color_cwd
  set -l prefix
  set -l suffix '>'
  if contains -- $USER root toor
      if set -q fish_color_cwd_root
          set color_cwd $fish_color_cwd_root
      end
      set suffix '#'
  end

  # If we're running via SSH, change the host color.
  set -l color_host $fish_color_host
  if set -q SSH_TTY
      set color_host $fish_color_host_remote
  end

  # Write pipestatus
  set -l prompt_status (__fish_print_pipestatus " [" "]" "|" (set_color $fish_color_status) (set_color --bold $fish_color_status) $last_pipestatus)

  echo -n -s "$USER" @ (set_color $color_host) (prompt_hostname) $normal ' ' (set_color $color_cwd) (prompt_pwd) $normal $prompt_status $suffix ' '
end

function fish_right_prompt -d 'Write out the right prompt'
  __fish_git_prompt
end

if test (uname) = 'Linux'
  function open -d 'Linux shim to function like OSX `open`'
    xdg-open $argv > /dev/null
  end

  function pbcopy -d 'Linux shim to function like OSX `pbcopy`'
    xclip -sel clip $argv
  end
end

function git-tree --description 'Shortcut to show nice git tree'
  git log --all --graph --oneline --decorate $argv
end

function git_subdirs_do
  echo ''
  for dir in (ls)
    if test -d "$dir/.git"
      echo $dir
      echo ''
      pushd $dir
      $argv
      popd
      echo ''
    end
  end
end

function fetch -d 'git fetch all subdirectories'
  function f
    git fetch; git -c color.status=always status -sb | head -1
  end
  echo 'Fetching...'
  git_subdirs_do f
end


function prune -d 'git prune all subdirectories'
  function f
    git prune; and git remote prune origin
  end
  echo 'Pruning...'
  git_subdirs_do f
end

function grep -d 'Colour wrapper around normal grep'
  command grep --colour=always $argv
end

