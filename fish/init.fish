echo 'Loading OrochiConfig...'

set script_dir (dirname (status --current-filename))
. "$script_dir"/util.fish
. "$script_dir"/dir-env.fish

echo 'Loaded OrochiConfig'

