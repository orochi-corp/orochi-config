#!/usr/bin/env sh

git clone --depth 1 https://github.com/dag/vim-fish
rm -rf vim-fish/LICENSE vim-fish/README.md vim-fish/.git

mkdir -p ~/.vim
cp -r vim-fish/* ~/.vim/

rm -rf vim-fish
