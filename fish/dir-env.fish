function dir-env -d 'set env by env.fish file. Usage: dir-env <proj-name> [parent-group=dius]'
  set proj "$argv[1]"
  set -q 'argv[2]'; and set parent "$argv[2]"; or set parent 'dius'
  
  set env_file ~/"Development/$parent/$proj/env.fish"
  if test -f "$env_file"
    echo "Sourcing dir env ($parent/$proj)..."
    . "$env_file"
  else
    echo "File not found for dir env ($env_file)"
  end
end

# TODO: clean up old env

