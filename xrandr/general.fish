function displays_current_verbose -d 'List all displays and modes'
  xrandr --current
end

function displays_current -d 'List all displays with current mode'
  xrandr --current | grep '*\|\bconnected'
end

