function tv_off -d 'Laptop on, DP1 off'
  xrandr --output eDP1 --auto \
         --output DP1  --off
end

function tv_on -d 'Laptop on, DP1 on above'
  xrandr --output eDP1 --auto \
         --output DP1  --auto --above eDP1
end

