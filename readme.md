# Config for Orochi-class machines

This is probably not what you want :)

## Getting started

Clone this repo, then activate selected modules.

Assuming you already have `fish` installed, and set as default shell.
If not, check [the docs](http://fishshell.com/docs/current/tutorial.html#switching-to-fish).

## Modules

These are modules insofar as they encapsulate separate concerns.

### Fish utils and dir env

Source `fish/init.fish` in `~/.config/fish/config.fish`.
Set `dir-env`, if that's a thing for you.

Add `vim` syntax highlighting for `fish` scripts with `fish/install-vim-syntax.fish`.

### App config

Link everything in `dotfiles` with the link script.

### Platform-specific SDKs

Check the readme for each subdir in `sdk`.

## To add

- [ ] JDK
- [ ] Android SDK
- [ ] Flutter SDK

## Not included

Don't forget to grab:

- `~/.ssh/config`
- `pass` or `password-store`
  - `gpg --export[-secret-keys]` from old and `gpg --import` to new.
- `jq`
- `bat`
- `docker` + `docker-compose`
- `htop`

