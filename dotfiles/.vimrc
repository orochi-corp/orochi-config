set shell=/bin/bash
syntax enable
filetype plugin indent on

set tabstop=2
set shiftwidth=2
set expandtab
autocmd BufWritePre * %s/\s\+$//e
