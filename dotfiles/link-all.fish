set script_dir (pushd (dirname (status --current-filename)); pwd; popd)

echo 'Linking files from '$script_dir' to ~'
for file in (ls -A --file-type "$script_dir" | grep -v fish)
  echo "-> $file"
  ln -s "$script_dir"/$file ~/$file
end

