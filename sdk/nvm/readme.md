# Node Version Manager

The nicest and lightest way to install `nvm` for `fish` is
[fish-nvm](https://github.com/jorgebucaran/fish-nvm).

```
fisher add jorgebucaran/fish-nvm
```

This requires [fisher](https://github.com/jorgebucaran/fisher)
(a lightweight package manager) to be installed, if it's not already.

> This looks like a scary `curl` piped straight into your shell, so read the script first.
